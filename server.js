// Modules import

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mysql = require('mysql');
const session = require('express-session')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require("dotenv").config();

// BDD connection

const connection = mysql.createConnection({
  host     : process.env.DB_HOST,
  user     : process.env.DB_USER,
  password : process.env.DB_PASSWORD,
  database : process.env.DB_DATABASE
});

connection.connect((err) => {
    if(err) {
        console.log(err);
        return;
    }
    console.log(`${process.env.DB_DATABASE} is online !`)
});

// Set session

app.use(session({
    secret: 'colibri',
}));

// Set view engine

app.set("view engine", "pug");

app.use(express.static(`${__dirname}/static`));

// Application

    // Admin

        // Display home page for admin

app.get("/admin/home", (req, res) => {
    connection.query(`SELECT title, idchallenges FROM challenges;`, 
    (err, title, fields) => {
        if(err) {
            console.log(err);
            return;
        }
        connection.query(`SELECT pseudo, idusers FROM users;`, 
        (err, user, fields) => {
            if(err) {
                console.log(err);
                return;
            }
            res.render("posts/admin_home", {titles: title, users: user, head: req.session.pseudo});
        });
    });
});

        // Learn more admin

app.post("/admin/learn/more", (req, res) => {
    console.log(req.body);
    connection.query(`SELECT * FROM challenges WHERE title = "${req.body.title}";`, 
    (err, result, fields) => {
        if(err) {
            console.log(err);
        }
        console.log(result);
        connection.query(`SELECT content, date, pseudo, challenges_idchallenges FROM comments INNER JOIN users ON idusers = users_idusers;`,
        (err, comment, fields) => {
            if(err) {
                console.log(err);
            }
            console.log(comment);
            let goodcomment = [];
            comment.forEach((element, i) => {
                console.log(element);
                console.log(element.challenges_idchallenges);
                console.log(result[0].idchallenges);
                if (element.challenges_idchallenges == result[0].idchallenges) {
                    goodcomment.push(element);
                    console.log("=>" + goodcomment);
                }
            });
            console.log(goodcomment);
            res.render("posts/admin_challenge", {challenges: result, comments: goodcomment, head: req.session.pseudo});
        });
    });
});

        // Delete challenges

app.post("/delete/challenge", (req, res) => {
    console.log(req.body.pseudo);

            // Delete posts of this challenge

    connection.query(`DELETE FROM comments WHERE challenges_idchallenges = "${req.body.idchallenge}";`, 
    (err, result, fields) => {
        if(err) {
            console.log(err);
            return;
        }
        console.log(result);
    });

            // Delete challenge

    connection.query(`DELETE FROM challenges WHERE idchallenges = "${req.body.idchallenge}";`, 
    (err, result, fields) => {
        if(err) {
            console.log(err);
            return;
        }
        console.log(result);
    });
    res.redirect("/admin/home");
});

        // Ban users

app.post("/ban", (req, res) => {
    console.log(req.body.pseudo);

            // Delete posts of this user

    connection.query(`DELETE FROM comments WHERE users_idusers = "${req.body.iduser}";`, 
    (err, result, fields) => {
        if(err) {
            console.log(err);
            return;
        }
        console.log(result);
    });

            // Delete user

    connection.query(`DELETE FROM users WHERE idusers = "${req.body.iduser}";`, 
    (err, result, fields) => {
        if(err) {
            console.log(err);
            return;
        }
        console.log(result);
    });
    res.redirect("/admin/home");
});

        // Display form challenge

app.get("/create/challenge", (req, res) => {
    res.render("posts/create_challenge", {head: req.session.pseudo});
});


    // Users

        // Sign in & session

app.get("/", (req, res) => {
    res.render("posts/login");
});

app.post("/connection", (req, res) => {
    let email = req.body.email;
    let pwd = req.body.pwd;
    connection.query(`SELECT * FROM users WHERE email = "${req.body.email}"`, 
    (err, result, fields) => {
        console.log(result);
        if (typeof result[0] == 'object' && result[0].password == req.body.password) {
            req.session.pseudo = result[0].pseudo;
            req.session.iduser = result[0].idusers;
            console.log(req.session);
            // Check si admin
            if (result[0].admin == 1) {
                res.redirect("/admin/home");  
            }
            else {
                res.redirect("/home");
            }
        }
        else {
            res.redirect('/');
        }
    });
});

        // Sign up

app.get("/register", (req, res) => {
    res.render("posts/register");
});

        // Forgotten password

app.get("/newpassword", (req, res) => {
    res.render("posts/new_password");
});

app.post("/change/password", (req, res) => {
    connection.query(`UPDATE users SET password = "${req.body.password}" WHERE pseudo = "${req.body.pseudo}"`, 
    (err, result, fields) => {
        if(err) {
            console.log(err);
            return;
        }
        res.redirect("/");
    });
})

        // Home page

app.get("/home", (req, res) => {
    connection.query(`SELECT title FROM challenges`, 
    (err, result, fields) => {
        if(err) {
            console.log(err);
            return;
        }
        res.render("posts/home", {titles: result, head: req.session.pseudo});
    });
});

        // Display challenges

app.get("/challenge", (req, res) => {
    connection.query(`SELECT * FROM challenges`, 
    (err, result, fields) => {
        if(err) {
            console.log(err);
            return;
        }
        res.render("posts/challenge", {head: req.session.pseudo});
    });
});

        // Learn more user

app.post("/learn/more", (req, res) => {
    console.log(req.body);
    connection.query(`SELECT * FROM challenges WHERE title = "${req.body.title}";`, 
    (err, result, fields) => {
        if(err) {
            console.log(err);
        }
        console.log(result);
        connection.query(`SELECT content, date, pseudo, challenges_idchallenges FROM comments INNER JOIN users ON idusers = users_idusers;`,
        (err, comment, fields) => {
            if(err) {
                console.log(err);
            }
            console.log(comment);
            let goodcomment = [];
            comment.forEach((element, i) => {
                console.log(element);
                console.log(element.challenges_idchallenges);
                console.log(result[0].idchallenges);
                if (element.challenges_idchallenges == result[0].idchallenges) {
                    goodcomment.push(element);
                    console.log("=>" + goodcomment);
                }
            });
            console.log(goodcomment);
            res.render("posts/challenge", {challenges: result, comments: goodcomment, head: req.session.pseudo});
        });
    });
});

    // Store BDD

        // Users

    app.post("/store/users", (req, res) => {
        connection.query(`INSERT INTO users (pseudo, email, password) VALUES ('${req.body.pseudo}', '${req.body.email}', '${req.body.password}');`, 
        (err, result, fields) => {
            if(err) {
                console.log(err);
            }
            res.redirect("/");
        });
    });

        // Challenges

app.post("/store/challenge", (req, res) => {
    console.log(req.body);
    connection.query(`INSERT INTO challenges (title, content) VALUES ('${req.body.title}', '${req.body.content}');`, 
        (err, result, fields) => {
            if(err) {
                console.log(err);
            }
    });
    res.redirect("/admin/home");
});

        // Comments

app.post("/store/comment", (req, res) => {
    connection.query(`INSERT INTO comments (content, users_idusers, challenges_idchallenges) VALUES ("${req.body.content}", "${req.session.iduser}", "${req.body.idchallenges}");`, 
        (err, result, fields) => {
            if(err) {
                console.log(err);
            }
    });
    res.redirect("/home");
})

// Run server

app.listen(process.env.PORT, () => console.log(`listen on ${process.env.PORT}`));

